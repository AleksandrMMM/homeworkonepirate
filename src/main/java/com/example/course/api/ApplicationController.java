package com.example.course.api;
import com.example.course.model.Person;
import org.springframework.web.bind.annotation.*;

import javax.print.DocFlavor;

@RestController
@RequestMapping("items") // базовый uri всех методов контроллера для получения параметров запроса
public class ApplicationController {

    // GET использующий  @RequestParam
    public String getItemById(@RequestParam(name = "id", required = false) Long id) {

        return "Возвращаем элемент с id " + id;
    }

    // POST запрос, использующий @RequestBody для получения  тела запроса
    @PostMapping
    public String createItem(@RequestBody Person person) {

        return "Создаем новый элемент " + person;
    }

    //PUT запрос, использующий @PathVariable для получения переменной из URI
    @PutMapping("{id}")
    public String updateItem(@PathVariable("id") Long id, @RequestBody String item) {
        return "обновляем элемент с id " + id + " на " + item;
    }

    // DELETE запрос, использующий @PathVariable для получения переменной из URI
    @DeleteMapping("{id}")
    public String deleteItem(@PathVariable("id") Long id ) {
        return " удаляем элемент с id " + id;
    }










}
