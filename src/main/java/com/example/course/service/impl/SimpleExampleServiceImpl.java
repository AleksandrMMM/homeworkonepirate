package com.example.course.service.impl;

import org.springframework.context.annotation.Primary;

@Primary
public class SimpleExampleServiceImpl implements ExampleService {
    @Override
    public void printInformation() {
        System.out.println("print to SIMPLE");
    }
}
