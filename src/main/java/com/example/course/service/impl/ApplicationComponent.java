package com.example.course.service.impl;

import com.example.course.config.properties.ApplicationProperties;
import jakarta.servlet.http.PushBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ApplicationComponent {

    @Value(value = "${app.description}")
    private String propertyValue;

    private final ExampleService exampleService;

    private final ApplicationProperties applicationProperties;


    public void executePrint() {
        exampleService.printInformation();
        System.out.println(applicationProperties.getName());
    }
}
