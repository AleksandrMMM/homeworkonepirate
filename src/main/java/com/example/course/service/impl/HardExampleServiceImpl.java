package com.example.course.service.impl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class HardExampleServiceImpl implements ExampleService {

    @Transactional
    @Override
    public void printInformation() {
        System.out.println("print info from HARD");
    }
}
