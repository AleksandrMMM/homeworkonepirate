package com.example.course.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfig {

    @Bean
    @Primary
    public RestTemplate simpleRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public RestTemplate hardRestTemplate() {

        return new RestTemplate();
    }

}
