package com.example.course;

import com.example.course.service.impl.ApplicationComponent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@ConfigurationPropertiesScan
public class CourseApplication {

    private ApplicationComponent applicationComponent;

    public CourseApplication(ApplicationComponent applicationComponent) {
        this.applicationComponent = applicationComponent;

        applicationComponent.executePrint();
    }

    public static void main(String[] args) {
        SpringApplication.run(CourseApplication.class, args);
    }

}
